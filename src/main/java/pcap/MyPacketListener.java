package pcap;

import org.pcap4j.core.PacketListener;
import org.pcap4j.packet.Packet;
import pcap.packethandlers.dns.DnsRequestPacketHandler;
import pcap.packethandlers.tcp.NewTcpConnectionHandler;
import pcap.packethandlers.PacketHandler;

import java.util.Arrays;
import java.util.Collection;

public class MyPacketListener implements PacketListener {

	private final Collection<PacketHandler> handlers = Arrays.asList(
		new NewTcpConnectionHandler(),
		new DnsRequestPacketHandler());

	@Override
	public void gotPacket(Packet packet) {
		for (PacketHandler handler : handlers) {
			if (handler.applies(packet)) {
				try {
					handler.handle(packet);
				} catch (Exception e) {
					System.err.println(packet);
					e.printStackTrace(System.err);
				}
			}
		}
	}
}

