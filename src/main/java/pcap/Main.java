package pcap;

import org.pcap4j.core.PcapHandle;
import org.pcap4j.core.PcapNetworkInterface;
import org.pcap4j.core.Pcaps;

public class Main {

	private static final int READ_TIMEOUT = 10; // [ms]
	private static final int SNAPLEN = 65536; // [bytes]
	private static final PcapNetworkInterface.PromiscuousMode MODE = PcapNetworkInterface.PromiscuousMode.NONPROMISCUOUS;
	private static final int LOOP_INFINITE = -1;

	public static void main(String[] args) throws Exception {
		PcapNetworkInterface networkInterface = Pcaps.getDevByName(args[0]);
		PcapHandle handle = networkInterface.openLive(SNAPLEN, MODE, READ_TIMEOUT);

		handle.loop(LOOP_INFINITE, new MyPacketListener());
	}

}
