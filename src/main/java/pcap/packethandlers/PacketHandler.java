package pcap.packethandlers;

import org.pcap4j.packet.Packet;

public interface PacketHandler {

	boolean applies(Packet packet);

	void handle(Packet packet);
}
