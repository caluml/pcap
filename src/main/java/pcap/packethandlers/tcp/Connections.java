package pcap.packethandlers.tcp;

import java.net.InetAddress;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Connections {

	private final List<TcpConnection> connections = new ArrayList<>();

	public TcpConnection add(Instant now,
													 InetAddress src,
													 int srcPort,
													 InetAddress dst,
													 int dstPort) {
		TcpConnection tcpConnection = new TcpConnection(now, src, srcPort, dst, dstPort);
		connections.add(tcpConnection);
		return tcpConnection;
	}

	public Optional<TcpConnection> remove(InetAddress src,
																				int srcPort,
																				InetAddress dst,
																				int dstPort) {
		TcpConnection tcpConnection = new TcpConnection(Instant.now(), src, srcPort, dst, dstPort);
		int i = connections.indexOf(tcpConnection);
		if (i > -1) {
			TcpConnection removed = connections.remove(i);
			return Optional.of(removed);
		}

		return Optional.empty();
	}
}
