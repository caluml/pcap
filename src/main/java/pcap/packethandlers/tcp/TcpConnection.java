package pcap.packethandlers.tcp;

import java.net.InetAddress;
import java.time.Instant;

public record TcpConnection(Instant start,
														InetAddress src, int srcPort,
														InetAddress dst, int dstPort) {

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		TcpConnection that = (TcpConnection) o;

		if (srcPort != that.srcPort) return false;
		if (dstPort != that.dstPort) return false;
		if (src != null ? !src.equals(that.src) : that.src != null) return false;
		return dst != null ? dst.equals(that.dst) : that.dst == null;
	}

	@Override
	public int hashCode() {
		int result = start != null ? start.hashCode() : 0;
		result = 31 * result + (src != null ? src.hashCode() : 0);
		result = 31 * result + srcPort;
		result = 31 * result + (dst != null ? dst.hashCode() : 0);
		result = 31 * result + dstPort;
		return result;
	}

	@Override
	public String toString() {
		return "TcpConnection: " + start + " " + src + ":" + srcPort + "-> " + dst + ":" + dstPort;
	}
}
