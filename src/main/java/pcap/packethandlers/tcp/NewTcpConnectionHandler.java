package pcap.packethandlers.tcp;

import org.pcap4j.packet.IpPacket;
import org.pcap4j.packet.Packet;
import org.pcap4j.packet.TcpPacket;
import pcap.packethandlers.PacketHandler;

import java.time.Instant;
import java.util.Optional;

public class NewTcpConnectionHandler implements PacketHandler {

	private final Connections connections = new Connections();

	@Override
	public boolean applies(Packet packet) {
		return packet.contains(TcpPacket.class);
	}

	@Override
	public void handle(Packet packet) {
		Instant now = Instant.now();
		try {
			Packet ipPacket = packet.getPayload();
			IpPacket.IpHeader ip = (IpPacket.IpHeader) ipPacket.getHeader();
			TcpPacket tcpPacket = (TcpPacket) ipPacket.getPayload();
			TcpPacket.TcpHeader tcp = tcpPacket.getHeader();
			if (tcp.getSyn() &&
				tcp.getAck() &&
				!tcp.getRst() &&
				!tcp.getUrg() &&
				!tcp.getPsh() &&
				!tcp.getFin()) {

				TcpConnection tcpConnection = connections.add(now, ip.getDstAddr(), tcp.getDstPort().valueAsInt(), ip.getSrcAddr(), tcp.getSrcPort().valueAsInt());
				System.out.println("Opened: " + tcpConnection);
			} else if (tcp.getAck() && tcp.getFin()) {
				Optional<TcpConnection> optionalConnection = connections.remove(ip.getDstAddr(), tcp.getDstPort().valueAsInt(), ip.getSrcAddr(), tcp.getSrcPort().valueAsInt());
				optionalConnection.ifPresent(old -> {
					System.out.println("Closed: " + old + " (open for " + (now.toEpochMilli() - old.start().toEpochMilli()) + " ms)");
				});
			}
		} catch (Exception e) {
			e.printStackTrace(System.err);
		}
	}

}
