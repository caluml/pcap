package pcap.packethandlers.dns;

import org.pcap4j.packet.DnsPacket;
import org.pcap4j.packet.Packet;
import pcap.packethandlers.PacketHandler;

public class DnsRequestPacketHandler implements PacketHandler {

	@Override
	public boolean applies(Packet packet) {
		return packet.contains(DnsPacket.class);
	}

	@Override
	public void handle(Packet packet) {
		DnsPacket dnsPacket = packet.get(DnsPacket.class);
		DnsPacket.DnsHeader dns = dnsPacket.getHeader();
		if (!dns.isResponse()) {
			dns.getQuestions().forEach(question -> {
				System.out.println("DNS: " + question.getQName().getName() + " " + question.getQType().name());
			});
		}
	}
}
